const express = require('express');
const path = require('path');

const app = express();

const PORT = process.env.PORT || 5000;

app.use(express.static(path.join(__dirname,"src/public")));

app.get('/', (req,res) => {
    res.sendFile(path.join(__dirname,"src/public/index.html"));
});

app.get('/landing', (req,res) => {
    res.sendFile(path.join(__dirname,"src/public/landing.html"));
});

app.get('/problem1', (req,res) => {
    res.sendFile(path.join(__dirname,"src/public/output","1-matches-per-year.json"));
});

app.get('/problem2', (req,res) => {
    res.sendFile(path.join(__dirname,"src/public/output","2-matches-won-per-team-per-year.json"));
});

app.get('/problem3', (req,res) => {
    res.sendFile(path.join(__dirname,"src/public/output","3-extra-runs-conceded.json"));
});

app.get('/problem4', (req,res) => {
    res.sendFile(path.join(__dirname,"src/public/output","4-top-10-economical-bowlers.json"));
});

app.get('/problem5', (req,res) => {
    res.sendFile(path.join(__dirname,"src/public/output","5-team-won-toss-and-match.json"));
});

app.get('/problem6', (req,res) => {
    res.sendFile(path.join(__dirname,"src/public/output","6-highest-man-of-match.json"));
});

app.get('/problem7', (req,res) => {
    res.sendFile(path.join(__dirname,"src/public/output","7-strike-rate-batsmen.json"));
});

app.get('/problem8', (req,res) => {
    res.sendFile(path.join(__dirname,"src/public/output","8-highest-times-player-dismissed.json"));
});

app.get('/problem9', (req,res) => {
    res.sendFile(path.join(__dirname,"src/public/output","9-bowler-with-best-economy.json"));
});

app.listen(PORT, () => {
    console.log(`Listening at port ${PORT}`);
});