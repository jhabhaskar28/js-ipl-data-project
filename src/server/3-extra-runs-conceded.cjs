function extraRunsConceded(matchesJson,deliveriesJson){

    let matchIdOf2016Array = matchesJson.filter((match) => {
        return match.season === "2016";
    })
    .map((match) => {
        return match.id;
    });

    let matchesOf2016 = deliveriesJson.filter(match => {
        return matchIdOf2016Array.includes(match.match_id);
    });

    let extraRunsPerTeam = matchesOf2016.reduce(function(accumulator,currentValue){

        if(accumulator[currentValue.bowling_team] === undefined){
            accumulator[currentValue.bowling_team] = parseInt(currentValue.extra_runs);
        } else {
            accumulator[currentValue.bowling_team] += parseInt(currentValue.extra_runs);
        }

        return accumulator;
    },{});

    return extraRunsPerTeam;
}

module.exports = extraRunsConceded;