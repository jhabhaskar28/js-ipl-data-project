function highestTimesPlayerDismissed(deliveriesJson){
    let playerDismissalObject = {};

    let highestTimesEachPlayerDismissed = deliveriesJson.reduce(function(accumulator,currentValue){

        if(currentValue.player_dismissed !== ""){

            if(playerDismissalObject[currentValue.player_dismissed] === undefined && currentValue.player_dismissed !== ""){
                playerDismissalObject[currentValue.player_dismissed] = {max: 0};
                accumulator[currentValue.player_dismissed] = 0;
            }
            if(playerDismissalObject[currentValue.player_dismissed][currentValue.bowler] === undefined){
                playerDismissalObject[currentValue.player_dismissed][currentValue.bowler] = 1;
            } else {
                playerDismissalObject[currentValue.player_dismissed][currentValue.bowler] += 1;
            }
            if(playerDismissalObject[currentValue.player_dismissed][currentValue.bowler] > playerDismissalObject[currentValue.player_dismissed]["max"]){

                playerDismissalObject[currentValue.player_dismissed]["max"] = playerDismissalObject[currentValue.player_dismissed][currentValue.bowler];
                accumulator[currentValue.player_dismissed] = playerDismissalObject[currentValue.player_dismissed]["max"];
            }
            
        }

        return accumulator;
    },{});

    return highestTimesEachPlayerDismissed;
}

module.exports = highestTimesPlayerDismissed;