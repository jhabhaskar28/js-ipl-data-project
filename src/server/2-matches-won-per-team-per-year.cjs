function matchesWonPerTeamYearWise(matchesJson){
    
    let mathesWonPerTeamPerYear = matchesJson.reduce((accumulator,currentValue) => {
        if(currentValue.winner !== ""){
            if(accumulator[currentValue.winner] === undefined){
                accumulator[currentValue.winner] = {};
            }
            if(accumulator[currentValue.winner][currentValue.season] === undefined){
                accumulator[currentValue.winner][currentValue.season] = 1;
            } else {
                accumulator[currentValue.winner][currentValue.season] += 1;
            }
        }
        
        return accumulator;
    },{});    
    
    return mathesWonPerTeamPerYear;
}

module.exports = matchesWonPerTeamYearWise;