function bowlerWithBestEconomy(deliveriesJson){

    let superOverMatches = deliveriesJson.filter((match) => {
        return match.is_super_over === "1";
    });

    let oversBowledByBowlers = {};

    let runsGivenByBowlers = superOverMatches.reduce((accumulator,currentValue) => {

        if(accumulator[currentValue.bowler] === undefined){
            accumulator[currentValue.bowler] = parseInt(currentValue.total_runs);
            oversBowledByBowlers[currentValue.bowler] = 1;
        } else {
            accumulator[currentValue.bowler] += parseInt(currentValue.total_runs);
            if(currentValue.ball === "1"){
                oversBowledByBowlers[currentValue.bowler] += 1;
            }
        }

        return accumulator;
    },{});

    let bowlerEconomyObject = superOverMatches.reduce((accumulator,currentValue) => {

        if(accumulator[currentValue.bowler] === undefined){
            accumulator[currentValue.bowler] = runsGivenByBowlers[currentValue.bowler]/oversBowledByBowlers[currentValue.bowler];
        }

        return accumulator;
    },{});

    let bowlerEconomyArray = Object.keys(bowlerEconomyObject)
    .map((bowler) => {
        return [bowler,bowlerEconomyObject[bowler]];
    });

    bowlerEconomyArray.sort((bowler1,bowler2) => {
        
        if(bowler1[1]>bowler2[1]){
            return 1;
        } else if(bowler1[1]<bowler2[1]){
            return -1;
        } else {
            if(bowler1[0]>bowler2[0]){
                return 1;
            } else {
                return -1;
            }
        }
    });

    return bowlerEconomyArray[0][0];
}

module.exports = bowlerWithBestEconomy;