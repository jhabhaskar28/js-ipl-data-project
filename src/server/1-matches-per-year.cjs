function matchesPerYear(matchesJson){

    let matchesPerYear = matchesJson.reduce(function(accumulator,currentValue) {
            
        if(accumulator[currentValue.season]===undefined){
            accumulator[currentValue.season] = 1;
        } else {
            accumulator[currentValue.season] += 1;
        }

        return accumulator;
    },{});

    return matchesPerYear;
}

module.exports = matchesPerYear;