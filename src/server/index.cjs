const csvtojson = require('csvtojson');
const fs = require('fs');
const path = require('path');

const csvFilePath = path.join(__dirname,"../data/matches.csv");
const csvFilePath2 = path.join(__dirname,"../data/deliveries.csv");

csvtojson().fromFile(csvFilePath).then((matchesJson) => {
    csvtojson().fromFile(csvFilePath2).then((deliveriesJson) => {

        const matchesPerYear = require('./1-matches-per-year.cjs');
        const countOfMatchesPerYear = matchesPerYear(matchesJson);
        const outputPath1 = path.join(__dirname,"../public/output/1-matches-per-year.json");
        fs.writeFileSync(outputPath1,JSON.stringify(countOfMatchesPerYear));

        const matchesWonPerTeamYearWise = require('./2-matches-won-per-team-per-year.cjs');
        const mathesWonPerTeamPerYear = matchesWonPerTeamYearWise(matchesJson);
        const outputPath2 = path.join(__dirname,"../public/output/2-matches-won-per-team-per-year.json");
        fs.writeFileSync(outputPath2,JSON.stringify(mathesWonPerTeamPerYear));

        const extraRunsConceded = require('./3-extra-runs-conceded.cjs');
        const extraRunsPerTeam = extraRunsConceded(matchesJson,deliveriesJson);
        const outputPath3 = path.join(__dirname,"../public/output/3-extra-runs-conceded.json");
        fs.writeFileSync(outputPath3,JSON.stringify(extraRunsPerTeam));

        const topTenEconomicalBowlers = require('./4-top-10-economical-bowlers.cjs');
        const top10BowlerEconomies = topTenEconomicalBowlers(matchesJson,deliveriesJson);
        const outputPath4 = path.join(__dirname,"../public/output/4-top-10-economical-bowlers.json");
        fs.writeFileSync(outputPath4,JSON.stringify(top10BowlerEconomies));

        const teamWonTossAndMatch = require('./5-team-won-toss-and-match.cjs');
        const matchAndTossWin = teamWonTossAndMatch(matchesJson);
        const outputPath5 = path.join(__dirname,"../public/output/5-team-won-toss-and-match.json");
        fs.writeFileSync(outputPath5,JSON.stringify(matchAndTossWin));

        const highestPlayerOfMatch = require('./6-highest-man-of-match.cjs');
        const seasonWiseHighestPlayerOfMatch = highestPlayerOfMatch(matchesJson);
        const outputPath6 = path.join(__dirname,"../public/output/6-highest-man-of-match.json");
        fs.writeFileSync(outputPath6,JSON.stringify(seasonWiseHighestPlayerOfMatch));
        
        const strikeRateBatsmen = require('./7-strike-rate-batsmen.cjs');
        const batsmenStrikeRate = strikeRateBatsmen(matchesJson,deliveriesJson);
        const outputPath7 = path.join(__dirname,"../public/output/7-strike-rate-batsmen.json");
        fs.writeFileSync(outputPath7,JSON.stringify(batsmenStrikeRate));

        const highestTimesPlayerDismissed = require('./8-highest-times-player-dismissed.cjs');
        const highestTimesEachPlayerDismissed = highestTimesPlayerDismissed(deliveriesJson);
        const outputPath8 = path.join(__dirname,"../public/output/8-highest-times-player-dismissed.json");
        fs.writeFileSync(outputPath8,JSON.stringify(highestTimesEachPlayerDismissed));

        const bowlerWithBestEconomy = require('./9-bowler-with-best-economy.cjs');
        const bowlerEconomyResult = bowlerWithBestEconomy(deliveriesJson);
        const outputPath9 = path.join(__dirname,"../public/output/9-bowler-with-best-economy.json");
        fs.writeFileSync(outputPath9,JSON.stringify(bowlerEconomyResult));

    });
});