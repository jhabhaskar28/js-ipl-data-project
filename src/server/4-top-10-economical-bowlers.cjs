function topTenEconomicalBowlers(matchesJson,deliveriesJson) {

    let matchIdsOf2015 = matchesJson.filter((match) => {
        return match.season === "2015";
    })
    .map((match) => {
        return match.id;
    });

    let matchesOf2015 = deliveriesJson.filter((match) => {
        return matchIdsOf2015.includes(match.match_id);
    });
    
    let bowlerRunsGiven = matchesOf2015.reduce((accumulator,currentValue) => {
        if(accumulator[currentValue.bowler] === undefined){
            accumulator[currentValue.bowler] = parseInt(currentValue.total_runs);
        } else {
            accumulator[currentValue.bowler] += parseInt(currentValue.total_runs);
        }
        return accumulator;
    },{});

    let bowlerOvers = matchesOf2015.reduce((accumulator,currentValue) => {
        if(currentValue.wide_runs === "0" && currentValue.noball_runs === "0"){
            if(accumulator[currentValue.bowler] === undefined){
                accumulator[currentValue.bowler] = 1/6;
            } else {
                accumulator[currentValue.bowler] += 1/6;
            }
        }
        return accumulator;
    },{});

    let bowlerEconomyObject = matchesOf2015.reduce((accumulator,currentValue) => {
        if(accumulator[currentValue.bowler] === undefined){
            accumulator[currentValue.bowler] = bowlerRunsGiven[currentValue.bowler]/bowlerOvers[currentValue.bowler];
        }
        return accumulator;
    },{});

    let bowlerEconomyArray = Object.keys(bowlerEconomyObject)
    .map(bowlerEconomy => {
        return [bowlerEconomy,bowlerEconomyObject[bowlerEconomy]];
    });

    bowlerEconomyArray.sort((value1,value2) => {
        
        if(value1[1]>value2[1]){
            return 1;
        } else {
            return -1;
        }
    });

    let top10BowlerEconomies = bowlerEconomyArray.slice(0,10);

    return top10BowlerEconomies;
}

module.exports = topTenEconomicalBowlers;