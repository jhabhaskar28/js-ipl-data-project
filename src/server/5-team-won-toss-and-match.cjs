function teamWonTossAndMatch(matchesJson){

    let matchAndTossWin = matchesJson.reduce(function(accumulator,currentValue){

        if(currentValue.winner === currentValue.toss_winner){
            
            if(accumulator[currentValue.winner] === undefined){
                accumulator[currentValue.winner] = 1;
            } else {
                accumulator[currentValue.winner] += 1;
            }
        }

        return accumulator;
    },{});

    return matchAndTossWin;
}

module.exports = teamWonTossAndMatch;