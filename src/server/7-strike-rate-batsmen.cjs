function strikeRateBatsmen(matchesJson,deliveriesJson){
    let matchIdAndSeasons = matchesJson.reduce((accumulator,currentValue) => {
        accumulator[currentValue.id] = currentValue.season;  
        return accumulator;
    },{});

    let batsmenTotalBalls = {};

    let batsmenTotalRuns = deliveriesJson.reduce(function(accumulator,currentValue){
        let season = matchIdAndSeasons[currentValue.match_id];
        if(accumulator[season] === undefined){
            accumulator[season] = {};
            batsmenTotalBalls[season] = {};
        }
        if(accumulator[season][currentValue.batsman] === undefined){
            accumulator[season][currentValue.batsman] = parseInt(currentValue.batsman_runs);
            batsmenTotalBalls[season][currentValue.batsman] = 1;
        } else {
            accumulator[season][currentValue.batsman] += parseInt(currentValue.batsman_runs);
            batsmenTotalBalls[season][currentValue.batsman] += 1;
        }

        return accumulator;
    },{});

    let batsmenStrikeRate = deliveriesJson.reduce(function(accumulator,currentValue){
        let season = matchIdAndSeasons[currentValue.match_id];
        if(accumulator[season] === undefined){
            accumulator[season] = {};
        }
        if(accumulator[season][currentValue.batsman] === undefined){
            accumulator[season][currentValue.batsman] = (batsmenTotalRuns[season][currentValue.batsman]/batsmenTotalBalls[season][currentValue.batsman])*100;
        }
        return accumulator;
    },{});

    return batsmenStrikeRate;
}

module.exports = strikeRateBatsmen;