function highestPlayerOfMatch(matchesJson){

    let iplManOfMatches = {};
    let seasonWiseHighestPlayerOfMatch = matchesJson.reduce(function(accumulator,currentValue) {

        if(iplManOfMatches[currentValue.season] === undefined){
            iplManOfMatches[currentValue.season] = {max: 0};
            accumulator[currentValue.season] = "";
        }
        if(currentValue.player_of_match !== ""){
            
            if(iplManOfMatches[currentValue.season][currentValue.player_of_match] === undefined){
                iplManOfMatches[currentValue.season][currentValue.player_of_match] = 1;
            } else {
                iplManOfMatches[currentValue.season][currentValue.player_of_match] += 1;
            }

            if(iplManOfMatches[currentValue.season][currentValue.player_of_match] > iplManOfMatches[currentValue.season]["max"]){

                iplManOfMatches[currentValue.season]["max"] = iplManOfMatches[currentValue.season][currentValue.player_of_match];
                accumulator[currentValue.season] = currentValue.player_of_match;
            } else if(iplManOfMatches[currentValue.season][currentValue.player_of_match] === iplManOfMatches[currentValue.season]["max"]){

                if(currentValue.player_of_match <  accumulator[currentValue.season]){
                    accumulator[currentValue.season] = currentValue.player_of_match;
                }
            }
        }

        return accumulator;
    },{});

    return seasonWiseHighestPlayerOfMatch;
}

module.exports = highestPlayerOfMatch;